@extends('themes.default1.admin.layout.admin')
@section('HeadInclude')
@stop
<!-- header -->
@section('PageHeader')
<h1>{!! Lang::get('lang.admin_panel') !!}</h1>
@stop
<!-- /header -->
<!-- breadcrumbs -->
@section('breadcrumbs')
@stop

<style type="text/css">
    
    .content-wrapper { min-height: auto !important; }

    .settingiconblue {
        padding-top: 0.5rem;
        padding-bottom: 0.5rem;
    }

     .settingdivblue {
        width: 80px;
        height: 80px;
        margin: 0 auto;
        text-align: center;
        border: 5px solid #C4D8E4;
        border-radius: 100%;
        padding-top: 5px;
    }

     .settingdivblue span { margin-top: 3px; }

     .fw_600 { font-weight: 600; }
    .settingiconblue p{
        text-align: center;
        font-size: 16px;
        word-wrap: break-word;
        font-variant: small-caps;
        font-weight: 500;
        line-height: 30px;
    }
</style>
<!-- /breadcrumbs -->
<!-- content -->
@section('content')
<!-- failure message -->
@if(Session::has('fails'))
<div class="alert alert-danger alert-dismissable">
    <i class="fas fa-ban"> </i> <b> {!! Lang::get('lang.alert') !!}! </b>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {{Session::get('fails')}}
</div>
@endif

<div class="card card-light">
    <div class="card-header">
        <h3 class="card-title">{!! Lang::get('lang.staffs') !!}</h3>
    </div>
    <!-- /.box-header -->
    <div class="card-body">
        <div class="row">

            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                    {{-- <div class="settingdivblue">
                        <a href="{{ url('agents') }}">
                            <span class="fa-stack fa-2x">
                                <i class="fas fa-user fa-stack-1x"></i>
                            </span>
                        </a>
                    </div> --}}
                    <div class="text-center text-sm"><a href="{{ url('agents') }}"><i class="fas fa-user"> </i> {!! Lang::get('lang.agents') !!}</a></div>
                </div>
            </div>

            <div class="col-md-2 col-sm-6">
                    <div class="settingiconblue">
                        
                        <div class="text-center text-sm"><a href="{{ url('departments') }}"><i class="fas fa-sitemap"></i> {!! Lang::get('lang.departments') !!}</a></div>
                    </div>
                </div>

                <div class="col-md-2 col-sm-6">
                    <div class="settingiconblue">
                        
                        <div class="text-center text-sm"><a href="{{ url('teams') }}"><i class="fas fa-users"></i> {!! Lang::get('lang.teams') !!}</a></div>
                    </div>
                </div>

                <div class="col-md-2 col-sm-6">
                    <div class="settingiconblue">
                       
                        <div class="text-center text-sm"><a href="{{ url('groups') }}"><i class="fas fa-object-group"></i> {!! Lang::get('lang.groups') !!}</a></div>
                    </div>
                </div>
        </div>
    </div>
</div>

<div class="card card-light">
    
    <div class="card-header">
        <h3 class="card-title">{!! Lang::get('lang.email') !!}</h3>
    </div>
    
    <div class="card-body">
        
        <div class="row">
            <!--col-md-2-->
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                  
                    <div class="text-center text-sm"><a href="{{ url('emails') }}"><i class="fas fa-envelope"></i> {!! Lang::get('lang.emails') !!}</a></div>
                </div>
            </div>
            <!--/.col-md-2-->
            <!--col-md-2-->
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                    
                    <div class="text-center text-sm"><a href="{{ url('banlist') }}"><i class="fas fa-ban"></i> {!! Lang::get('lang.ban_lists') !!}</a></div>
                </div>
            </div>
            <!--/.col-md-2-->
            <!--col-md-2-->
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                    
                    <div class="text-center text-sm"><a href="{{ url('template-sets') }}"><i class="fas fa-reply"></i> {!! Lang::get('lang.templates') !!}</a></div>
                </div>
            </div>
            <!--/.col-md-2-->
            <!--/.col-md-2-->
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                   
                    <div class="text-center text-sm"><a href="{{url('getemail')}}"><i class="fas fa-at"></i> {!! Lang::get('lang.email-settings') !!}</a></div>
                </div>
            </div>

            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                   
                    <div class="text-center text-sm"><a href="{{url('queue')}}"><i class="fas fa-upload"></i> {!! Lang::get('lang.queues') !!}</a></div>
                </div>
            </div>

            <!--col-md-2-->
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                    
                    <div class="text-center text-sm"><a href="{{ url('getdiagno') }}"><i class="fas fa-plus"></i> {!! Lang::get('lang.diagnostics') !!}</a></div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- ./box-body -->
</div>

<div class="card card-light">
    
    <div class="card-header">
        <h3 class="card-title">{!! Lang::get('lang.manage') !!}</h3>
    </div>
    
    <!-- /.box-header -->
    <div class="card-body">
        <div class="row">
            <!--/.col-md-2-->
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                   
                    <div class="text-center text-sm"><a href="{{url('helptopic')}}"><i class="fas fa-file-alt"></i> {!! Lang::get('lang.help_topics') !!}</a></div>
                </div>
            </div>
            <!--/.col-md-2-->
            <!--/.col-md-2-->
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                    
                    <div class="text-center text-sm"><a href="{{url('sla')}}"><i class="fas fa-clock"></i> {!! Lang::get('lang.sla_plans') !!}</a></div>
                </div>
            </div>
            <!--/.col-md-2-->

            <!--/.col-md-2-->
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                   
                    <div class="text-center text-sm"><a href="{{url('forms')}}"><i class="fas fa-file-alt"></i> {!! Lang::get('lang.forms') !!}</a></div>
                </div>
            </div>
            <!--/.col-md-2-->
            <!--/.col-md-2-->
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                    
                    <div class="text-center text-sm"><a href="{{url('workflow')}}"><i class="fas fa-sitemap"></i> {!! Lang::get('lang.workflow') !!}</a></div>
                </div>
            </div>
                <!-- priority -->
             <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                    
                    <div class="text-center text-sm"><a href="{{url('ticket/priority')}}"><i class="fas fa-asterisk"></i> {!! Lang::get('lang.priority') !!}</a></div>
                </div>
            </div>
            <!--/.col-md-2-->
            <!--/.col-md-2-->
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                   
                    <div class="text-center text-sm"><a href="{{url('url/settings')}}"><i class="fas fa-server"></i> Url</a></div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- ./box-body -->
</div>

<div class="card card-light">
    
    <div class="card-header">
        <h3 class="card-title">{!! Lang::get('lang.ticket') !!}</h3>
    </div>
    <!-- /.box-header -->
    <div class="card-body">
        <div class="row">
                
            <!--/.col-md-2-->
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                   
                    <div class="text-center text-sm"><a href="{{url('getticket')}}"><i class="fas fa-file-alt"></i> {!! Lang::get('lang.ticket') !!}</a></div>
                </div>
            </div>
                <!--/.col-md-2-->
            
             <!--/.col-md-2-->
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                    
                    <div class="text-center text-sm"><a href="{{url('getresponder')}}"><i class="fas fa-reply-all"></i> {!! Lang::get('lang.auto_response') !!}</a></div>
                </div>
            </div>
            <!--/.col-md-2-->
            
            <!--/.col-md-2-->
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                    
                    <div class="text-center text-sm"><a href="{{url('getalert')}}"><i class="fas fa-bell"></i> {!! Lang::get('lang.alert_notices') !!}</a></div>
                </div>
            </div>
            <!--/.col-md-2-->
            
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                    
                    <div class="text-center text-sm"><a href="{{url('setting-status')}}"><i class="fas fa-plus-square"></i> Statuses</a></div>
                </div>
            </div>
                            
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                    
                    <div class="text-center text-sm"><a href="{{url('getratings')}}"><i class="fas fa-star"></i> {!! Lang::get('lang.ratings') !!}</a></div>
                </div>
            </div>

            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                    
                    <div class="text-center text-sm"><a href="{{url('close-workflow')}}"><i class="fas fa-sitemap"></i> {!! Lang::get('lang.close_ticket_workflow') !!}</a></div>
                </div>
            </div>
           <?php \Event::fire('settings.ticket.view',[]); ?>

        </div>
        <!-- /.row -->
    </div>
    <!-- ./box-body -->
</div>
<div class="card card-light">
    <div class="card-header">
        <h3 class="card-title">{!! Lang::get('lang.settings') !!}</h3>
    </div>
    <!-- /.box-header -->
    <div class="card-body">
        <div class="row">
            <!--/.col-md-2-->
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                    
                    <div class="text-center text-sm"><a href="{!! url('getcompany') !!}"><i class="fas fa-building"></i> {!! Lang::get('lang.company') !!}</a></div>
                </div>
            </div>
            <!--/.col-md-2-->
            <!--/.col-md-2-->
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                    
                    <div class="text-center text-sm"><a href="{{url('getsystem')}}"><i class="fas fa-laptop"></i> {!! Lang::get('lang.system') !!}</a></div>
                </div>
            </div>
            <!--/.col-md-2-->

            
            <!--/.col-md-2-->
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                    
                    <div class="text-center text-sm"><a href="{{ url('social/media') }}"><i class="fas fa-globe"></i> {{Lang::get('lang.social-login')}}</a></div>
                </div>
            </div>
            <!--/.col-md-2-->
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                   
                    <div class="text-center text-sm"><a href="{{url('languages')}}"><i class="fas fa-language"></i> {!! Lang::get('lang.language') !!}</a></div>
                </div>
            </div>
            <!--/.col-md-2-->
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                    
                    <div class="text-center text-sm"><a href="{{url('job-scheduler')}}"><i class="fas  fa-hourglass"></i> {!! Lang::get('lang.cron') !!}</a></div>
                </div>
            </div>
            <!--/.col-md-2-->
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                    
                    <div class="text-center text-sm"><a href="{{url('security')}}"><i class="fas fa-lock"></i> {!! Lang::get('lang.security') !!}</a></div>
                </div>
            </div>
            <!--/.col-md-2-->
            
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                    
                    <div class="text-center text-sm"><a href="{{url('settings-notification')}}"><i class="fas fa-bell"></i> {!! Lang::get('lang.notifications') !!}</a></div>
                </div>
            </div>
            
            <?php \Event::fire('settings.system',[]); ?>
     
        </div>
        <!-- /.row -->
    </div>
    <!-- ./box-body -->
</div>

<div class="card card-light">
    <div class="card-header">
        <h3 class="card-title">{!! Lang::get('lang.error-debug') !!}</h3>
    </div>
    <!-- /.box-header -->
    <div class="card-body">
        <div class="row">
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                    
                    <div class="text-center text-sm"><a href="{{ route('err.debug.settings') }}"><i class="fas fa-bug"></i> {!! Lang::get('lang.debug-options') !!}</a></div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- ./box-body -->
</div>


<div class="card card-light">
    <div class="card-header">
        <h3 class="card-title">{!! Lang::get('lang.widgets') !!}</h3>
    </div>
    <!-- /.box-header -->
    <div class="card-body">
        <div class="row">
            <!--/.col-md-2-->
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                    
                    <div class="text-center text-sm"><a href="{{ url('widgets') }}"><i class="fas fa-list-alt "></i> {!! Lang::get('lang.widgets') !!}</a></div>
                </div>
            </div>
            <!--/.col-md-2-->                                        
            <!--col-md-2-->
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                    
                    <div class="text-center text-sm"><a href="{{ url('social-buttons') }}"><i class="fas fa-cubes"></i> {!! Lang::get('lang.social') !!}</a></div>
                </div>
            </div>
            
        </div>
        <!-- /.row -->
    </div>
    <!-- ./box-body -->
</div>

<div class="card card-light">
    <div class="card-header">
        <h3 class="card-title">{!! Lang::get('lang.plugin') !!}</h3>
    </div>
    <!-- /.box-header -->
    <div class="card-body">
        <div class="row">
            <!--/.col-md-2-->
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                    
                    <div class="text-center text-sm"><a href="{{ url('plugins') }}"><i class="fas fa-plug"></i> {!! Lang::get('lang.plugin') !!}</a></div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- ./box-body -->
</div>
<div class="card card-light">
    <div class="card-header">
        <h3 class="card-title">{!! Lang::get('lang.api') !!}</h3>
    </div>
    <!-- /.box-header -->
    <div class="card-body">
        <div class="row">
            <!--/.col-md-2-->
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                    
                    <div class="text-center text-sm"><a href="{{ url('api') }}"><i class="fas fa-cogs"></i> {!! Lang::get('lang.api') !!}</a></div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- ./box-body -->
</div>
<div class="card card-light">
    <div class="card-header">
        <h3 class="card-title">Logs</h3>
    </div>
    <!-- /.box-header -->
    <div class="card-body">
        <div class="row">
            <!--/.col-md-2-->
            <div class="col-md-2 col-sm-6">
                <div class="settingiconblue">
                    
                    <div class="text-center text-sm"><a href="{{ url('logs') }}"><i class="fas fa-lock"></i> Logs</a></div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- ./box-body -->
</div>
<?php \Event::fire('service.desk.admin.settings', array()); ?>


@stop
